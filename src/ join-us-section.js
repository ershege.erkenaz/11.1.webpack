const joinUsSection = (function() {
    const footer = document.getElementsByClassName('app-footer')[0];
    const joinSection = document.createElement('div');
    const mainText = document.createElement('h1');
    const secondaryText = document.createElement('p');
    const emailForm = document.createElement("FORM");
    const emailInput = document.createElement("INPUT");
    const emailButton = document.createElement("BUTTON");

    // Setting elements
    joinSection.setAttribute('id', 'join');
    mainText.setAttribute('id', 'mainText');
    secondaryText.setAttribute('id', 'secondaryText');

    emailForm.setAttribute("id", "myForm");
    emailInput.setAttribute("id", "emailInput");
    emailInput.setAttribute("type", "text");
    emailInput.setAttribute("placeholder", "Email");
    emailButton.setAttribute('id', 'emailButton');
    emailButton.innerHTML = "SUBSCRIBE";
    mainText.innerHTML = "Join Our Program";
    secondaryText.innerHTML = `Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua!`;

    emailForm.addEventListener("submit", function(e){
        console.log(emailInput.value);
        e.preventDefault();
        emailInput.value = "";
    }, false);

    // Appending elements
    joinSection.appendChild(mainText);
    joinSection.appendChild(secondaryText);
    joinSection.appendChild(emailForm);
    
    document.getElementById("myForm").appendChild(emailInput);
    document.getElementById("myForm").appendChild(emailButton);

    // Function to append join section to the body and before the footer
    function appendJoinSection() {
        document.body.appendChild(joinSection);
        footer.parentNode.insertBefore(joinSection, footer);
    }

    // Public interface
    return {
        appendJoinSection: appendJoinSection
    };
})();

export default joinUsSection;